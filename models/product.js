const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['variants', 'imageURL', 'category', 'dimensions', 'size'];
const productSchema = {
    hashKey: 'collectionType',
    rangeKey: 'collectionId',
    timestamps: true,
    schema: Joi.object({
        collectionType: 'product', //Product
        collectionId: Joi.string().alphanum(), //ProductId
        title: Joi.string(),
        description: Joi.string(),
        price: Joi.number().positive().min(1),
        variants: Joi.object(),
        imageURL: Joi.string(),
        category: Joi.array(),
        dimensions: Joi.object(),
        size: Joi.string()
    }).optionalKeys(optionalParams).unknown(true)
}


const attToGet = ['title', 'description', 'variants', 'price', 'imageURL', 'category', 'dimensions', 'size'];
const attToQuery = ['description', 'imageURL', 'price', 'size', 'title', 'collectionId'];
const optionsObj = {
    attToGet,
    attToQuery,
};
const Product = SchemaModel(productSchema, optionsObj);

module.exports = Product;