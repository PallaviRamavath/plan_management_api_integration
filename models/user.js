const Joi = require('joi');
const SchemaModel = require('../config/schema');

const tableName = `userdetails`;
const userSchema = {
	hashKey: 'email',
	rangeKey:'username',
	timestamps: true,
	schema: Joi.object({
		email: Joi.string().email(),
		fullName: Joi.string(),
		username: Joi.string().alphanum(),
		password: Joi.string().min(6), //here encrypted password is saved
		storeName: Joi.string()
	}).unknown(true)
};
const User = SchemaModel(userSchema, {tableName: tableName});

module.exports = User;